# Pokemontrainer

This is a pokemon trainer app, where trainers can collect pokemon and view collected pokemons. This project is a Single Page Application created with the Angular framework.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.0.6.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


## Install

```
git clone "https://gitlab.com/FadiAkkaoui/pokemon-trainer.git"
cd pokemon-trainer
npm install
```

## Usage

``ng serve``

This will run a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

You need to create two environment files: environment.prod.ts and environment.ts. In these you need to add API urls and API keys. The environment.ts file should look as following:

```
export const environment = {
  production: true,
  apiUsers: <your url>,
  apiPokemons: "https://pokeapi.co/api/v2/pokemon?limit=50&offset=0",
  apiKey: <your key>,
};
```
The key can be found under config vars in the api application settings. Open the app and copy the api url and paste it to equal the url variable.
Restart the development server.

## Contributors
@FadiAkkaoui and @rebsan00003

## Contributing

No contributions allowed.
