import { Component, Input, OnInit } from '@angular/core';
import { Pokemon } from 'src/app/models/pokemon.model';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {
  
  get trainer(): Trainer | undefined {
    return this.trainerService.trainer;
  }

  //if trainer exists return trainers pokemon
  get pokemons(): Pokemon[] {
    if(this.trainerService.trainer){
      return this.trainerService.trainer.pokemon;
    }
    return [];
  }

  username?: string = "";
  constructor(
    private trainerService: TrainerService
  ) { }

  ngOnInit(): void {
    if(this.trainer !== undefined){
      this.username = this.trainer.username
    }
  }

}
