import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.model';


const { apiUsers, apiKey } = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  //Dependency Injection
  constructor(private readonly http: HttpClient) { }

  //If trainer does not exist then create a trainer, otherwise return trainer
  public login(username: string): Observable<Trainer> {
    return this.checkUsername(username)
      .pipe(
        switchMap((trainer: Trainer | undefined) => {
          if(trainer === undefined) {
            return this.createUser(username)
          }
          return of(trainer);
        })
      )
  }


  //Check if trainer exists
  private checkUsername(username: string): Observable<Trainer | undefined>{
    return this.http.get<Trainer[]>(`${apiUsers}?username=${username}`)
    .pipe(
      map((response: Trainer[]) => response.pop())
    )
  }


  //Create trainer with submitted username and set pokemon array to empty
  private createUser(username: string): Observable<Trainer> {
    const trainer = {
      username,
      pokemon: []
    };
    
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": apiKey
    });
    
    return this.http.post<Trainer>(apiUsers, trainer, {
      headers
    })
  }

}
