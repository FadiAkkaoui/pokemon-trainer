import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {

  private _trainer?: Trainer;

  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  //save trainer to session storage
  set trainer(trainer: Trainer | undefined){
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!)
    this._trainer = trainer;
  }

  constructor() { 
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
  }

  //check if selected pokemon is already collected by trainer
  public inCollected(pokemonId: number): boolean {
    if(this._trainer){
      return Boolean(this.trainer?.pokemon.find((pokemon: Pokemon) => pokemon.id === pokemonId));
    }
    return false;
  }

  //adds selected pokemon to trainers pokemons
  public addToCollected(pokemon: Pokemon): void {
    if(this._trainer){
      this._trainer.pokemon.push(pokemon)
    }
  }

  //removes selected pokemon from trainers pokemons
  public removeFromCollected(pokemonId: number): void {
    if(this._trainer){
      this._trainer.pokemon = this._trainer.pokemon.filter((pokemon: Pokemon) => pokemon.id !== pokemonId);
    }
  }
}
