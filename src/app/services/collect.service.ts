import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { TrainerService } from './trainer.service';

const { apiKey, apiUsers } = environment;

@Injectable({
  providedIn: 'root'
})
export class CollectService {

  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonCatalogueService,
    private readonly trainerService: TrainerService,
  ) { }



  //Patch request with the trainer id and pokemon id
  public addToPokemons(pokemonId: number): Observable<Trainer> {
    if (!this.trainerService.trainer){
      throw new Error("addToPokemons: There is no trainer");
    }

    const trainer: Trainer = this.trainerService.trainer;

    const pokemon: Pokemon| undefined = this.pokemonService.pokemonById(pokemonId);

    if(!pokemon){
      throw new Error("addToPokemons: No pokemon with id: " + pokemonId)
    }

    //If selected pokemon is already collected then remove it from trainers pokemons, else add to collected pokemons
    if(this.trainerService.inCollected(pokemonId)){
      this.trainerService.removeFromCollected(pokemonId);
    }else {
      this.trainerService.addToCollected(pokemon);
    }

    const headers = new HttpHeaders({
      'content-type': 'application/json',
      'x-api-key': apiKey
    })

    //Adds trainers pokemons to API 
    return this.http.patch<Trainer>(`${apiUsers}/${trainer.id}`,{
      pokemon: [...trainer.pokemon] 
    }, {
      headers
    })
    .pipe(
      tap((updatedTrainer: Trainer) => {
        this.trainerService.trainer = updatedTrainer;
      })
    )
  }


}
