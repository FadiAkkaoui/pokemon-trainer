import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { NotExpr } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, Pokemondata } from '../models/pokemon.model';

const { apiPokemons } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCatalogueService {

  private _pokemons: Pokemon[] = [];
  private _error: string = "";
  private _loading: boolean = false;

  get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  
  constructor(private readonly http: HttpClient) {}

  //Get all pokemons from pokeapi and assign id to each pokemon
  public findAllPokemons(): void {

    if(this._pokemons.length > 0 || this.loading){
      return;
    }

    this._loading = true;
    this.http.get<Pokemondata>(apiPokemons)
    .pipe(
      finalize(() => {
        this._loading = false;
      })
    )
    .subscribe({
      next: (pokemondata: Pokemondata) => {
        const pokemons: Pokemon[] = pokemondata.results;
        this._pokemons = pokemons;
        for(let i = 0; i < pokemons.length; i++){
          const urlArr = pokemons[i].url.split("/");
          let pokemonId = urlArr[urlArr.length - 2];
          pokemons[i].id = parseInt(pokemonId);

        }
      },
      error: (error: HttpErrorResponse) => {
        this._error = error.message;
      }
    })
  }

  //Find pokemon by its id
  public pokemonById(id: number): Pokemon | undefined {
    return this._pokemons.find((pokemon: Pokemon) => pokemon.id === id)
  }
}

