//Interface for Pokemon
export interface Pokemon {
    name: string;
    url: string;
    id: number;
}

//Interface for handling results
export interface Pokemondata {
    results: Array<Pokemon>;
}




