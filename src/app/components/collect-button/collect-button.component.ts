import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit, Type } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.model';
import { CollectService } from 'src/app/services/collect.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-collect-button',
  templateUrl: './collect-button.component.html',
  styleUrls: ['./collect-button.component.css']
})
export class CollectButtonComponent implements OnInit {

  public loading: boolean = false;
  public isCollected: boolean = false;
  @Input() pokemonId: number = 0; 

  constructor(
    private trainerService: TrainerService,
    private readonly collectService: CollectService
  ) { }

  ngOnInit(): void {
    // Inputs are resolved
    this.isCollected = this.trainerService.inCollected(this.pokemonId);
  }

  // When trainer clicks on the collect button, adds the pokemon to list 
  onCollectedClick(): void {
    this.loading = true;
    this.collectService.addToPokemons(this.pokemonId)
      .subscribe({
        next: (trainer: Trainer) => {
          this.loading = false;
          this.isCollected = this.trainerService.inCollected(this.pokemonId);
        },
        error: (error: HttpErrorResponse) => {
          console.log("Error", error.message);
        }
      })
  }

}
